import { StreamingService } from '../services/StreamingService'
import { Socket } from 'socket.io'
import { StreamingApi } from '../types/streaming-api'
import { LoginFailResponse } from '../responses/LoginFailResponse'
import { User } from '../models/User'
import { LoginSuccessResponse } from '../responses/LoginSuccessResponse'
import { ROOM_LOBBY } from "../rooms/LobbyRoom";

export type ILoginReq = {
  id: string
}

export const isLoginReq = (data: any): data is ILoginReq => {
  return typeof data.id === 'string'
}


export class LoginListener {
  public constructor (
    public streamingService: StreamingService,
    public socket: Socket
  ) {
    this.socket.on(StreamingApi.LOGIN_REQ, (data) => {
      if(isLoginReq(data)) {
        if(this.streamingService.users.has(data.id)) {
          return this.socket.emit(StreamingApi.LOGIN_RES, new LoginFailResponse(data.id))
        }

        this.streamingService.users.add(data.id, new User(data.id, socket))

        socket.join(ROOM_LOBBY)

        return this.socket.emit(StreamingApi.LOGIN_RES, new LoginSuccessResponse(data.id))
      }
    })
  }
}
