import { User } from '../models/User'
import { Socket } from 'socket.io'

export class UsersService {
  items = new Map<string, User>();
  sockets = new Map<string, string>();

  has(uuid: string): boolean {
    return this.items.has(uuid)
  }

  hasBySocketId(id: string) {
    const uuid = this.sockets.get(id)
    return uuid && this.has(uuid)
  }

  hasBySocket(socket: Socket) {
    return this.hasBySocketId(socket.id)
  }

  get(uuid: string): User | undefined {
    return this.items.get(uuid)
  }

  getBySocketId(id: string) {
    const uuid = this.sockets.get(id)
    return uuid ? this.get(uuid) : undefined
  }

  getBySocket(socket: Socket) {
    return this.getBySocketId(socket.id)
  }

  add(uuid: string, user: User) {
    this.items.set(uuid, user)
    this.sockets.set(user.socket.id, uuid)
  }

  remove(uuid: string) {
    const user = this.get(uuid)
    if(user) {
      this.items.delete(uuid)
      this.sockets.delete(user.socket.id)
    }
  }

  removeBySocketId(id: string) {
    const uuid = this.sockets.get(id)

    if(uuid) {
      this.remove(uuid)
    }
  }

  removeBySocket(socket: Socket) {
    return this.removeBySocketId(socket.id)
  }
}