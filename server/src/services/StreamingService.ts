import { UsersService } from './UsersService'
import { Server } from 'socket.io'
import { LoginListener } from '../listeners/LoginListener'
import { StreamsService } from "./StreamsService";
import { LobbyRoom } from "../rooms/LobbyRoom";

const SOCKET_LISTENERS = [
  LoginListener
]

export class StreamingService {
  streams: StreamsService
  users = new UsersService()
  lobbyRoom: LobbyRoom

    public constructor (
    public io: Server
  ) {
    this.streams = new StreamsService(this)
    this.lobbyRoom = new LobbyRoom(this)
  }

  public init() {
    this.io.on("connection", (socket) => {
      const listeners = SOCKET_LISTENERS.map((listener) => {
        new listener(this, socket)
      })

      socket.onAny((data) => {
        console.group("server.ts:29 - onAny");
        console.log(data);
        console.groupEnd();
      })

      socket.on("disconnect", () => {
        this.users.removeBySocket(socket)
      });
    });
  }
}
