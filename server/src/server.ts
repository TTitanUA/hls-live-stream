import fastify from "fastify";
import fastifyIO from "fastify-socket.io"
import path from 'path'
import { StreamingService } from './services/StreamingService'

const CLIENT_BUILD_PATH = path.join(__dirname, '..', '..', 'client', 'build')

const server = fastify();

// server.register(require('fastify-static'), {
//   root: CLIENT_BUILD_PATH,
//   prefix: '/',
// })

server.register(fastifyIO);

server.register(require('fastify-http-proxy'), {
  upstream: 'http://localhost:3000',
  prefix: '/', // optional
  http2: false // optional
})





server.ready().then(() => {
  const streamingService = new StreamingService(server.io)
  streamingService.init()
});

server.listen(3001);
