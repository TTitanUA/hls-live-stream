export class LoginFailResponse {
  error: string = ''
  public constructor (
    login: string
  ) {
    this.error = `Login: ${login}, already in use`
  }
}