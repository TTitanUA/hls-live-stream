import { Socket } from 'socket.io'

export class User {
  public constructor (
    public uuid: string,
    public socket: Socket
  ) {}
}