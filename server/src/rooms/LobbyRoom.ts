import { StreamingService } from "../services/StreamingService";
import { StreamingApi } from "../types/streaming-api";
import { LobbyUpdateStreams } from "../responses/LobbyUpdateStreams";

export const ROOM_LOBBY = 'lobby'

export class LobbyRoom {
  public constructor(
    public streamingService: StreamingService,
  ) {
    this.streamingService.io.of("/").adapter.on("join-room", (room, id) => {
      if(room === ROOM_LOBBY) {
        this.sendStreamList(id)
      }
    });
  }

  sendStreamList(id: string) {
    this.streamingService.io.to(id).emit(
      StreamingApi.LOBBY_UPDATE_STREAMS,
      new LobbyUpdateStreams(this.streamingService.streams.getActiveList())
    )
  }

  updateStreamList() {
    this.streamingService.io.to(ROOM_LOBBY).emit(
      StreamingApi.LOBBY_UPDATE_STREAMS,
      new LobbyUpdateStreams(this.streamingService.streams.getActiveList())
    )
  }
}
