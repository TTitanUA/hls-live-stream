export enum  StreamingApi {
  'LOGIN_REQ' = 'LOGIN_REQ',
  'LOGIN_RES' = 'LOGIN_RES',
  'LOBBY_UPDATE_STREAMS' = 'LOBBY_UPDATE_STREAMS',
}
