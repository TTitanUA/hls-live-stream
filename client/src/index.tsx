import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import SocketContext from "./contexts/SocketContext";
import StreamingContext from "./contexts/StreamingContext";

ReactDOM.render(
  <React.StrictMode>
    <SocketContext>
      <StreamingContext>
        <App />
      </StreamingContext>
    </SocketContext>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
