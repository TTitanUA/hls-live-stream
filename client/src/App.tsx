import React, { useMemo } from 'react';
import './App.css';
import Recorder from "./components/Recorder";
import useStreaming from "./hooks/useStreaming";
import Login from "./components/Login";
import Streams from "./components/Streams";
import Watch from "./components/Watch";

function App() {
  const streaming = useStreaming();

  const recorderComponent = useMemo(() => (
    streaming.data.login.logined && !streaming.data.streams.watch && (
      <Recorder />
    )
  ), [streaming.data.login]);

  const loginComponent = useMemo(() => (
    !streaming.data.login.logined && (
      <Login />
    )
  ), [streaming.data.login]);

  const streamsComponent = useMemo(() => (
    streaming.data.login.logined && !streaming.data.streams.watch && !streaming.data.streams.record && (
      <Streams />
    )
  ), [streaming.data]);

  const watchComponent = useMemo(() => (
    streaming.data.login.logined && streaming.data.streams.watch && (
      <Watch />
    )
  ), [streaming.data]);

  console.group("App.tsx:36 - App");
  console.log(streaming.data);
  console.groupEnd();
  return (
    <div className="App">
      <div>
        {recorderComponent}
        {loginComponent}
        {streamsComponent}
        {watchComponent}
      </div>
    </div>
  );
}


export default App;
