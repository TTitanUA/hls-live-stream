import React, { useEffect, useReducer, useState } from 'react'
import useSocket from "../hooks/useSocket";
import {
  reducer,
  defaultState,
  defaultActions,
  publicActions,
  listeners,
  TStreamingActions,
  TStreamingData
} from '../services/StremingService'



export const StreamingDataContext = React.createContext<TStreamingData>({...defaultState})
export const StreamingActionsContext = React.createContext<TStreamingActions>({...defaultActions})

const StreamingContext: React.FC = ({children}) => {
  const socket = useSocket()
  const [data, dispatch] = useReducer(reducer, {...defaultState});
  const [actions, setActions] = useState<TStreamingActions>({...defaultActions})

  useEffect(() => {
    if(socket) {
      setActions(publicActions(dispatch, socket, data))
    }
  }, [socket, data, dispatch, setActions])

  useEffect(() => {
    if(socket) {
      setActions(publicActions(dispatch, socket, data))
      listeners(dispatch, socket, data)
    }
  }, [socket, data, dispatch, setActions])

  return (
    <StreamingDataContext.Provider value={data}>
      <StreamingActionsContext.Provider value={actions}>
        {children}
      </StreamingActionsContext.Provider>
    </StreamingDataContext.Provider>
  )
}


export default StreamingContext
