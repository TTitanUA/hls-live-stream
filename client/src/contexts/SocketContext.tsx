import React, { useEffect, useState } from 'react'
import { io, Socket } from "socket.io-client";

export const SocketIoContext = React.createContext<Socket | undefined>(undefined)

const SocketContext: React.FC = ({children}) => {
  const [socket, setSocket] = useState<Socket>()

  useEffect(() => {
    const socket = io();
    socket.on("connect", () => {
      setSocket(socket)
    });
    socket.on("disconnect", () => {
      setSocket(socket)
    });
  }, [])

  return (
    <SocketIoContext.Provider value={socket}>
      {children}
    </SocketIoContext.Provider>
  )
}


export default SocketContext
