import React from 'react'
import useStreaming from "../hooks/useStreaming";


const Watch: React.FC = () => {
  const streaming = useStreaming();

  const handleOnConnect = (id: string) => {
    console.log(id)
  }

  return (
    <div>
      <h2>Watch</h2>
    </div>
  )
}




export default Watch
