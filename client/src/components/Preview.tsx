import React, { useEffect, useRef } from 'react'

type TPreviewProps = {
  stream: MediaStream | undefined
}


const Preview: React.FC<TPreviewProps> = ({stream}) => {
  const ref = useRef<HTMLVideoElement>(null);
  //@ts-ignore
  window['stream'] = stream

  useEffect(() => {
    if(stream && ref.current) {
      ref.current.srcObject = stream
      ref.current.play()
    }

  }, [stream, ref])

  return (
    <div>
      <video style={{width: '300px'}} ref={ref} />
    </div>
  )
}




export default Preview
