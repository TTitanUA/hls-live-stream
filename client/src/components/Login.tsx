import React, { FormEvent, useState } from 'react'
import useStreaming from "../hooks/useStreaming";


const Login: React.FC = () => {
  const streaming = useStreaming();
  const [id, setId] = useState(streaming.data.login.id || '')

  const handleOnSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    streaming.actions.actionLogin({id})
  }

  return (
    <div>
      <h2>Enter your login</h2>
      <form onSubmit={handleOnSubmit}>
        <input type="text" value={id} onChange={(e) => setId(e.target.value)} />
        <button type="submit">Send</button>
      </form>
    </div>
  )
}




export default Login
