import React, { useEffect, useMemo } from 'react'
import useDisplayCapture from "../hooks/useDisplayCapture";
import Preview from "./Preview";
import useSocket from "../hooks/useSocket";
import useStreaming from "../hooks/useStreaming";

interface IRecorder {

}


const Recorder: React.FC<IRecorder> = ({}) => {
  const {stream, onStart} = useDisplayCapture()
  const socket = useSocket()
  const streaming = useStreaming();



  useEffect(() => {


    if(stream && stream.active && !streaming.data.streams.record) {
      streaming.actions.actionRecordStart({
        stream
      })
    }

    if(!stream && streaming.data.streams.record) {
      streaming.actions.actionRecordStop({})
    }

    console.group("Recorder.tsx:23 - useEffect");
    // @ts-ignore
    console.log(window['stream'] = stream);
    console.groupEnd();
  }, [stream?.active, streaming])

  const startRecordComponent = useMemo(() =>
    socket && !stream && <button onClick={() => onStart()}>Record</button>,
    [stream, onStart, socket]
  );

  return (
    <div>
      {startRecordComponent}
      <div>
        {stream && <Preview stream={stream} />}
      </div>
    </div>
  )
}




export default Recorder
