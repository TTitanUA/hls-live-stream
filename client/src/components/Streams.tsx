import React from 'react'
import useStreaming from "../hooks/useStreaming";


const Streams: React.FC = () => {
  const streaming = useStreaming();

  const handleOnConnect = (id: string) => {
    console.log(id)
  }

  return (
    <div>
      <h2>Active streams</h2>
      <ul>
        {streaming.data.streams.items.map((stream) => (
          <li key={stream.id}>{stream.title} <button onClick={() => handleOnConnect(stream.id)}>Connect</button></li>
        ))}
      </ul>
    </div>
  )
}




export default Streams
