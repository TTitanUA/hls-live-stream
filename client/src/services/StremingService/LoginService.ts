import {
  TStreamingActionWrapper, TStreamingData, TStreamingDispatcher,
  TStreamingInnerAction,
  TStreamingListener,
  TStreamingReducer
} from "./index";
import { Socket } from "socket.io-client";
import { StreamingApi } from '../../types/streaming-api'

export enum INNER_ACTIONS {
  'LOGIN_SUCCESS' = 'LOGIN_SUCCESS',
  'LOGIN_FAIL' = 'LOGIN_FAIL'
}

export type TLoginSuccessInnerAction = {
  type: INNER_ACTIONS.LOGIN_SUCCESS,
  payload: {
    id: string,
  }
}

export type TLoginFailInnerAction = {
  type: INNER_ACTIONS.LOGIN_FAIL,
  payload: {
    error: string,
  }
}

export type TLoginState = {
  id: string | undefined
  logined: boolean
  error: string
}

export type TActionLoginArgs = {
  id: string
}

export type TLoginActionWrappers = {
  actionLogin: TStreamingActionWrapper<TActionLoginArgs>
}

export const isTLoginSuccessInnerAction = (action: TStreamingInnerAction): action is TLoginSuccessInnerAction => {
  return action.type === INNER_ACTIONS.LOGIN_SUCCESS && typeof action.payload?.id === 'string'
}

export const isTLoginFailInnerAction = (action: TStreamingInnerAction): action is TLoginFailInnerAction => {
  return action.type === INNER_ACTIONS.LOGIN_FAIL && typeof action.payload?.error === 'string'
}

const loginSuccessReducer: TStreamingReducer<TLoginSuccessInnerAction> = (state, action) => {
  window.localStorage.setItem('streaming.last_login', action.payload.id)

  return {
    ...state,
    login: {
      logined: true,
      id: action.payload.id,
      error: ''
    }
  }
}

const loginFailReducer: TStreamingReducer<TLoginFailInnerAction> = (state, action) => {
  return {
    ...state,
    login: {
      logined: false,
      id: '',
      error: action.payload.error
    }
  }
}

const reducer: TStreamingReducer = (state, action) => {
  if(isTLoginSuccessInnerAction(action)) {
    return loginSuccessReducer(state, action)
  }

  if(isTLoginFailInnerAction(action)) {
    return loginFailReducer(state, action)
  }

  return state
}

const actionLogin: TLoginActionWrappers['actionLogin'] = (dispatch, socket, data) => (payload) => {
  socket.emit(StreamingApi.LOGIN_REQ, payload)
}

const defaultState: TLoginState = {
  id: undefined,
  logined: false,
  error: ''
}

const loadState = (): TLoginState => {
  return {
    ...defaultState,
    id: window.localStorage.getItem('streaming.last_login') || undefined
  }
}

const listenerOnLogin: TStreamingListener = (dispatch, socket, data) => {
  socket.on(StreamingApi.LOGIN_RES, (data) => {
    if(typeof data.id === 'string') {
      dispatch({
        type: INNER_ACTIONS.LOGIN_SUCCESS,
        payload: data
      })
      socket.off(StreamingApi.LOGIN_RES)
    } else {
      dispatch({
        type: INNER_ACTIONS.LOGIN_FAIL,
        payload: data
      })
    }
  })
}



const wrapActions = (dispatch: TStreamingDispatcher, socket: Socket, data: TStreamingData) => (
  {
    actionLogin: actionLogin(dispatch, socket, data)
  }
)

const defaultActions = {
  actionLogin: () => {}
}

export default {
  reducer,
  defaultState: loadState(),
  defaultActions,
  wrapActions,
  listeners: [
    listenerOnLogin
  ]
}
