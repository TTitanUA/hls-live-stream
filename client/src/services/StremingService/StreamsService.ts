import {
  TStreamingActionWrapper,
  TStreamingData,
  TStreamingDispatcher,
  TStreamingInnerAction,
  TStreamingListener,
  TStreamingReducer
} from "./index";
import { StreamingApi } from "../../types/streaming-api";
import { Socket } from "socket.io-client";

export enum INNER_ACTIONS {
  'STREAMS_UPDATE' = 'STREAMS_UPDATE',
  'STREAMS_RECORD_START' = 'STREAMS_RECORD_START',
  'STREAMS_RECORD_READY' = 'STREAMS_RECORD_READY',
  'STREAMS_RECORD_STOP' = 'STREAMS_RECORD_STOP',
}
export type TStreamItem = {
  id: string
  title: string
}

export type TRecordItem = {
  stream: MediaStream,
  recorder: MediaRecorder,
}

export type TStreamsState = {
  items: TStreamItem[],
  watch: TStreamItem | undefined,
  record: TRecordItem | undefined
}

export type TStreamsUpdateInnerAction = {
  type: INNER_ACTIONS.STREAMS_UPDATE,
  payload: {
    items: TStreamItem[],
  }
}

export type TStreamsRecordStartInnerAction = {
  type: INNER_ACTIONS.STREAMS_RECORD_START,
  payload: {
    stream: MediaStream,
    recorder: MediaRecorder
  }
}
export type TStreamsRecordStopInnerAction = {
  type: INNER_ACTIONS.STREAMS_RECORD_STOP,
  payload: {}
}

export type TActionRecordStartArgs = {
  stream: MediaStream
}

export type TActionRecordStopArgs = {}

export type TStreamsActionWrappers = {
  actionRecordStart: TStreamingActionWrapper<TActionRecordStartArgs>
  actionRecordStop: TStreamingActionWrapper<TActionRecordStopArgs>
}

const defaultState: TStreamsState = {
  items: [],
  watch: undefined,
  record: undefined,
}

export const isTStreamsUpdateInnerAction = (action: TStreamingInnerAction): action is TStreamsUpdateInnerAction => {
  return action.type === INNER_ACTIONS.STREAMS_UPDATE &&
    Array.isArray(action.payload?.items) &&
    action.payload?.items.every(isStream)
}

export const isTStreamsRecordStartInnerAction = (action: TStreamingInnerAction): action is TStreamsRecordStartInnerAction => {
  return action.type === INNER_ACTIONS.STREAMS_RECORD_START &&
    action.payload.stream instanceof MediaStream &&
    action.payload.recorder instanceof MediaRecorder
}


export const isTStreamsRecordStopInnerAction = (action: TStreamingInnerAction): action is TStreamsRecordStopInnerAction => {
  return action.type === INNER_ACTIONS.STREAMS_RECORD_STOP
}

export const isStream = (data: any): data is TStreamItem => {
  return typeof data.id === 'string' && typeof data.title === 'string'
}

const updateStreamsReducer: TStreamingReducer<TStreamsUpdateInnerAction> = (state, action) => {
  return {
    ...state,
    streams: {
      ...state.streams,
      items: [...action.payload.items],
    }
  }
}

const recordStartReducer: TStreamingReducer<TStreamsRecordStartInnerAction> = (state, {payload: {stream, recorder}}) => {
  return {
    ...state,
    streams: {
      ...state.streams,
      record: {
        stream,
        recorder
      }
    }
  }
}

const recordStopReducer: TStreamingReducer<TStreamsRecordStopInnerAction> = (state, action) => {
  return {
    ...state,
    streams: {
      ...state.streams,
      record: undefined
    }
  }
}

const reducer: TStreamingReducer = (state, action) => {
  if(isTStreamsUpdateInnerAction(action)) {
    return updateStreamsReducer(state, action)
  }

  if(isTStreamsRecordStartInnerAction(action)) {
    return recordStartReducer(state, action)
  }

  if(isTStreamsRecordStopInnerAction(action)) {
    return recordStopReducer(state, action)
  }

  return state
}

const listenerOnStreamsUpdate: TStreamingListener = (dispatch, socket, data) => {
  socket.on(StreamingApi.LOBBY_UPDATE_STREAMS, (data) => {
    if(Array.isArray(data.items)) {
      dispatch({
        type: INNER_ACTIONS.STREAMS_UPDATE,
        payload: data
      })
    }
  })
}

const actionRecordStart: TStreamsActionWrappers['actionRecordStart'] = (dispatch, socket, data) => ({stream}) => {
  socket.emit(StreamingApi.RECORD_START, {})

  const recorder = new MediaRecorder(stream)

  recorder.ondataavailable = async (e) => {
    socket.emit(StreamingApi.RECORD_CHUNK, {
      chunk: await e.data.arrayBuffer()
    })
  }

  dispatch({
    type: INNER_ACTIONS.STREAMS_RECORD_START,
    payload: {
      recorder,
      stream
    }
  })
}

const actionRecordStop: TStreamsActionWrappers['actionRecordStop'] = (dispatch, socket, data) => () => {
  socket.emit(StreamingApi.RECORD_STOP, {})

  if(data.streams.record) {
    try {
      data.streams.record.stream.getTracks().forEach(track => track.stop())
      data.streams.record.recorder.stop()
    } catch (err) {}

    dispatch({
      type: INNER_ACTIONS.STREAMS_RECORD_STOP,
      payload: {}
    })
  }
}

const wrapActions = (dispatch: TStreamingDispatcher, socket: Socket, data: TStreamingData) => (
  {
    actionRecordStart: actionRecordStart(dispatch, socket, data),
    actionRecordStop: actionRecordStop(dispatch, socket, data)
  }
)

const defaultActions = {
  actionRecordStart: (payload: TActionRecordStartArgs) => {},
  actionRecordStop: () => {}
}


export default {
  reducer,
  defaultState: {...defaultState},
  defaultActions,
  wrapActions,
  listeners: [
    listenerOnStreamsUpdate
  ]
}
