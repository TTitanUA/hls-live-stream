import { Socket } from "socket.io-client";
import LoginService from './LoginService'
import StreamsService from './StreamsService'

export type TStreamingData = {
  login: typeof LoginService.defaultState,
  streams: typeof StreamsService.defaultState
}

export type TStreamingDispatcher = (action: TStreamingInnerAction) => void

export type TStreamingInnerAction = {
  type: string,
  payload: any
}

export type TStreamingActionArgs = any
export type TStreamingAction<T extends TStreamingActionArgs = TStreamingActionArgs> = (payload: T) => void

export type TStreamingActions = ReturnType<typeof LoginService.wrapActions> &
  ReturnType<typeof StreamsService.wrapActions>

export type TStreamingActionWrapper<T extends TStreamingActionArgs = TStreamingActionArgs> =
  (dispatch: TStreamingDispatcher, socket: Socket, data: TStreamingData) => TStreamingAction<T>




export type TStreamingListener = (dispatch: TStreamingDispatcher, socket: Socket, data: TStreamingData) => void

export type TStreamingReducer<T extends TStreamingInnerAction = TStreamingInnerAction> = (state: TStreamingData, action: T) => TStreamingData

export const defaultState: TStreamingData = {
  login: LoginService.defaultState,
  streams: StreamsService.defaultState
}

export const defaultActions: TStreamingActions = {
  ...LoginService.defaultActions,
  ...StreamsService.defaultActions
}



export const reducer: TStreamingReducer = (state, action) => {
  state = LoginService.reducer(state, action)
  state = StreamsService.reducer(state, action)
  return state
}


export const publicActions = (dispatch: TStreamingDispatcher, socket: Socket, data: TStreamingData) => {
  return {
    ...LoginService.wrapActions(dispatch, socket, data),
    ...StreamsService.wrapActions(dispatch, socket, data)
  }
}

export const listeners: TStreamingListener = (...args): void => {
  LoginService.listeners.forEach((listener) => listener(...args))
  StreamsService.listeners.forEach((listener) => listener(...args))
}
