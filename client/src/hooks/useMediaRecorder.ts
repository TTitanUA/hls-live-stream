import { useCallback, useState } from "react";

const useMediaRecorder = () => {
  const [recorder, setRecorder] = useState<MediaRecorder | undefined>(undefined);

  const onStreamReady = useCallback(
    (stream: MediaStream) => {
      setRecorder(new MediaRecorder(stream))
    },
    [setRecorder]
  );


  return {
    recorder,
    onStreamReady
  }
}

export default useMediaRecorder
