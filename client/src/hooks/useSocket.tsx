import React, { useContext } from 'react'
import { SocketIoContext } from "../contexts/SocketContext";


const useSocket = () => {
  return useContext(SocketIoContext);
}

export default useSocket
