import React, { useContext } from 'react'
import { StreamingActionsContext, StreamingDataContext } from "../contexts/StreamingContext";
import { TStreamingActions, TStreamingData } from "../services/StremingService";


const useStreaming = (): {actions: TStreamingActions, data: TStreamingData} => {
  const actions = useContext(StreamingActionsContext);
  const data = useContext(StreamingDataContext);

  return {
    actions,
    data
  }
}

export default useStreaming
