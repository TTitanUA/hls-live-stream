import { useCallback, useEffect, useState } from "react";

const useDisplayCapture = () => {
  const [stream, setStream] = useState<MediaStream | undefined>(undefined);

  useEffect(() => {
    if(!stream) {
      return
    }

    stream.addEventListener('inactive', () => {
      setStream(undefined)
    })
  }, [stream, setStream]);


  const onStart = useCallback(
    () => {
      navigator.mediaDevices.getDisplayMedia({video: true, audio: false})
        .then(setStream)
        .catch(console.warn)
    },
    [setStream]
  );


  return {
    stream,
    onStart
  }
}

export default useDisplayCapture
